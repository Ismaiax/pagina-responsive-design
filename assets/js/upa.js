

function debounce(func, wait = 20, immediate = true) {
	var timeout;
	return function() {
		var context = this,
			args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

const sliderImages = document.querySelectorAll('.slide-in');

if(sliderImages.length) {
	console.log(sliderImages);
	console.log( (sliderImages[0].height || sliderImages[0].clientHeight) );
}

function checkSlide(e) {
	sliderImages.forEach(sliderImage => {
		// halfway through the image
		const slideInAt = (window.scrollY + window.innerHeight) - (sliderImage.height||sliderImage.clientHeight) / 2;
		// bottom of the image
		const imageBottom = sliderImage.offsetTop + (sliderImage.height||sliderImage.clientHeight);
		const isHalfShown = slideInAt > sliderImage.offsetTop;
		const isNotScrolledPast = window.scrollY < imageBottom;
		if (isHalfShown && isNotScrolledPast) {
			sliderImage.classList.add('active');
		} else {
			sliderImage.classList.remove('active');
		}

		console.log(slideInAt);
	});
}

window.addEventListener('scroll', debounce(checkSlide, 7));


function enviar(e){
	e.preventDefault();
	return false;
}

$(function() {
    console.log( "%cBienvenidos a la UPA", "background-color: rgb(106,0,30); color:white; padding:7px 15px; display:inline-block; border-radius:20px; font-family:Arial" );



	$("#formulario").validate({
		messages: {
			name: {
				required: 'Tu nombre es requerido',
				minlength: "Parece que no indicaste tu nombre completo",
				maxlength : "Tu nombre no puede exceder los 15 caracteres"
			},
			mail: {
				required: "Tu correo es requerido",
				email : "Tu correo parece estar incorrecto"
			},
			info : {
				required : "Indica el motivo de tu mensaje"
			}
		},
		rules: {
			name: {
				required: true,
				minlength: 3,
				maxlength: 15,
				number: false
			},
			mail : {
				required: true,
				email : true
			}, 
			info : {
				required: true,
			}
		}
	});
});
