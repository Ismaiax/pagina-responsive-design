# Responsive design

Responsable del proyecto [Ismael Tello](isaiasismael.telloLI8@comunidadunir.net)
Aplicación web responsive design, basada en Bootstrap 5. La temática de la aplicación es una página básica informativa de la _Unidad de Protección Animal de Valle de Chalco_.
Cuenta con 3 secciones:
- Página de salutación (o página de inicio)
- Página de información sobre la unidad
- Página de contacto


## Tabla de contenido
- [Estructura del proyecto](#estructura-del-proyecto)
- [Changelog](#changelog)

# Estructura del proyecto
> Estructura de carpetas y archivos

## Raíz

    .
    ├── assets                  # Carpeta de activos
          ├── css               # Carpeta de estilos (Bootstrap y personalizados) 
          ├── js                # Carpeta de scripts (Bootstrap y personalizado)
          ├── img               # Carpeta de todos los recurosos gráficos
    ├── README.md
    ├── nostros.html
    ├── contacto.html
    └── index.html


### Assets

La carpeta `assets` contiene todos los recursos que utiliza la página (imágenes, hojas de estilos [archivos css] y scripts de lado del cliente [archivos js]).
Algunas funciones o efectos fueron tomados de otros autores, según el lenguaje se listan a continuación



#### Frameworks
- [Bootstrap 5](https://getbootstrap.com/)

#### CSS
- CSS Animate [Daniel Eden](https://animate.style/)

#### Javascript
- Función para efectos de animación con el scroll de [Hima Vincent](https://codemyui.com/image-slide-scroll-animation/)
- Validación de campos del formulario [jQuery Validate](https://jqueryvalidation.org/)

---



# Changelog
Comienzo del proyecto 14 de febrero de 2021
## [1.0.1] - 2021-02-25
### Changed
- Se agregan algunos atributos para validar los criterios de accesibilidad
- Se agrega jquery validate



## [1.0.0] - 2021-02-14
### Added
- Se genera el proyecto principal con toda la estructura descrita en el presente documento y archivos relacionados